using System.Net;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using SatisfactoryHttpDataLogger;

var builder = WebApplication.CreateBuilder(args);
var cfg = Config.Read();
cfg.Save();

SatisfactoryHttpDataLogger.Controllers.PushController.cfg = cfg;

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<KestrelServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();