using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace SatisfactoryHttpDataLogger; 

public class DataContainer {
    [JsonPropertyName("type")]
    public string Type;
    [JsonPropertyName("value")]
    public string Value;
}