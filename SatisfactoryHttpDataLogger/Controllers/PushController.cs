using System.Data.Common;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using Newtonsoft.Json;
using Npgsql;

namespace SatisfactoryHttpDataLogger.Controllers;

[ApiController]
[Route("/")]
public class PushController : ControllerBase {
    public static Config cfg;

    [HttpPost("push")]
    public void Get() {
        string jsonString;
        using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8)) {
            jsonString = reader.ReadToEnd();
        }

        Console.WriteLine(jsonString);
        DbConnection conn = cfg.Engine switch
        {
            "postgres" => new NpgsqlConnection(
                $"Username={cfg.DbAuth.User}; Password={cfg.DbAuth.Password}; Host={cfg.DbAuth.Host}; Database={cfg.DbAuth.Database}; Port={cfg.DbAuth.Port}"),
            "mysql" => new MySqlConnection(
                $"Username={cfg.DbAuth.User}; Password={cfg.DbAuth.Password}; Host={cfg.DbAuth.Host}; Database={cfg.DbAuth.Database}; Port={cfg.DbAuth.Port}"),
            _ => throw new NotImplementedException("Provider not implemented, please request support")
        };
        conn.Open();
        var data = JsonConvert.DeserializeObject<Dictionary<string, DataContainer>>(jsonString);
        string cols = "", values = "";
        foreach (var dp in data) {
            // Console.WriteLine($"ALTER TABLE {cfg.Table} ADD if not EXISTS {dp.Key} {dp.Value.Type}");
            string alterCommand = $"ALTER TABLE {cfg.Table} ADD if not EXISTS {dp.Key} {dp.Value.Type}";
            DbCommand boof = cfg.Engine switch
            {
                "postgres" => new NpgsqlCommand(alterCommand, (NpgsqlConnection) conn),
                "mysql" => new MySqlCommand(alterCommand, (MySqlConnection) conn)
            };
            boof.ExecuteNonQuery();
            bool first = false;
            if (cols.Length == 0) first = true;
            cols += (first?"":",")+dp.Key;
            values += (first?"":",")+dp.Value.Value;
        }
        // Console.WriteLine($"insert into testdata ({cols}) values ({values})");
        string insertCommand = $"insert into {cfg.Table} ({cols}) values ({values})";
        DbCommand boofTwo = cfg.Engine switch
        {
            "postgres" => new NpgsqlCommand(insertCommand, (NpgsqlConnection) conn),
            "mysql" => new MySqlCommand(insertCommand, (MySqlConnection) conn)
        };
        boofTwo.ExecuteNonQuery();
        conn.Close();
    }
}