using ArcaneLibs;

namespace SatisfactoryHttpDataLogger; 

public class Config : SaveableObject<Config> {
    public DbAuth DbAuth { get; set; } = new();
    public string Engine { get; set; } = "postgres";
    public string Table { get; set; } = "testdata"; 
}

public class DbAuth {
    public string Host { get; set; } = "localhost";
    public short Port { get; set; } = 5432;
    public string User { get; set; } = "postgres";
    public string Password { get; set; } = "postgres";
    public string Database { get; set; } = "grafanatest";
}